import React from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import HomeLogin from "../Login/Pages/Home";
import Aplications from "../Login/Pages/Aplications";
import Juntas from "../Junta/Pages/Home";
import Consejos from "../Consejo/Pages/Home"
import { isAuthenticated, isLogin, exitApp } from "./Auth";
import Page404 from "../Page404/Page404";



const PrivateSessionRoute = (props) => {
    if (isAuthenticated()) {
        //console.log("esta logeado ", props)
        return (
            (<Route {...props} />)
        )
    } else {
        //console.log("NO esta logeado")
        if (isLogin()) {
            return (
                <Redirect to="/sesiones/aplicaciones" />
            )
        } else {
            return (
                <Redirect to="/sesiones/login" />
            )
        }
    }
}

const PrivateAppsRoute = (props) => {
    if (isLogin()) {
        console.log("esta logeado ", props)
        exitApp();

        return (
            <Route {...props} />
        )

    } else {
        console.log("NO tiene token ")
        return (
            <Redirect to="/sesiones/login" />
        )
    }
}



export default () => (

    <Router>
        <Switch>
            <Route path='/sesiones/login' component={HomeLogin} />

            <PrivateAppsRoute path='/sesiones/aplicaciones' component={Aplications} />
            <PrivateSessionRoute path='/sesiones/junta' component={Juntas} />
            <PrivateSessionRoute path='/sesiones/consejo' component={Consejos} />
            <Route component={Page404} />
        </Switch>
    </Router>
)



