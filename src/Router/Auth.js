import jwtDecode from "jwt-decode";
import axios from "axios";
// import { Link } from 'react-router-dom'
import { basePath } from "../Config/Server";

/**
 * Elimina el token y recarga la pagina
 */
export function logout() {
    const apps = localStorage.getItem('accessToken');
    let existToken = (apps != null && apps.length > 0) ? true : false
    if (existToken) {
        const url = `${basePath}/cierraSesion`
        axios.post(url, {},
            {
                headers: {
                    'Authorization': localStorage.getItem("accessToken")
                }
            }).then(resp => {
                console.log("Exito !!  ", resp)
                localStorage.removeItem("accessToken")
                localStorage.removeItem("permisoSistemas")
                localStorage.removeItem("username")
                localStorage.removeItem("menu")
                window.location.reload();
            }).catch(err => {
                console.log("Error!  ", err)
            })
    } else {
        localStorage.removeItem("permisoSistemas")
        localStorage.removeItem("username")
        window.location.reload();
    }
}

/**
 * cierrra session de la pagina aplicaciones
 */
export function exitApp() {
    //console.log("elino el de token")
    if (localStorage.getItem("accessToken") != null) {
        console.log("tiene token ")
        const url = `${basePath}/cierraSesion`
        axios.post(url, {},
            {
                headers: {
                    'Authorization': localStorage.getItem("accessToken")
                }

            }).then(resp => {
                console.log("Exito !!  ", resp)
                localStorage.removeItem("accessToken")
                localStorage.removeItem("menu")
                return true;
            }).catch(err => {
                console.log("Error!  ", err)

                return false;
            })
    } else {
        return true;
    }
}

/**
 * verfifca que tenga aplicaciones y que sea valido 
 */
export function isLogin() {
    const apps = JSON.parse(localStorage.getItem('permisoSistemas'));
    try {
        return (apps.length != null && apps.length > 0 && apps[0].idSistema) ? true : false;
    } catch (error) {
        return false
    }
}

/**
 * verifica que tenga un token valido 
 * y tenga aplicaciones 
 */
export function isAuthenticated() {
    const token = localStorage.getItem('accessToken');
    try {
        let user = jwtDecode(token)
        return (user.sub != null && token != null) ? true : false
    } catch (error) {
        return false;
    }

}