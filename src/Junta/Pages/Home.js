import React from "react";

import Header from "../Componets/Header/Header";
import Footer from "../Componets/Footer/Footer";
import ProcessMenu from "../Componets/ProcessMenu/ProcessMenu";
//import Menu from "../Componets/Sider/Sider";
import { ReactComponent as Heder_Logo } from "../Assets/img/Logo.svg";
import { ReactComponent as Heder_Logo_User } from "../Assets/img/user.svg";
import jwtDecode from "jwt-decode";
import { Layout } from "antd";
const { Content } = Layout;

export default function Home() {



    const data = JSON.parse(localStorage.getItem("data"));
    console.log("................",data)

    const dataResponse = {
        "infoMenu": {
            "detalleSelec": null,
            "listaDetalles": null,
            "estadoSelec": null,
            "listaEstados": null,
            "distritoFedSelec": null,
            "listaDistritosFed": null,
            "distritoLocSelec": null,
            "listaDistritosLoc": null,
            "listaDistritosFedAux": null,
            "municipioSelec": null,
            "listaMunicipios": null,
            "listMenu": null,
        },
        "idSistema": 2,
        "idEstado": data.idEstado,
        "idDistrito": data.idDistrito,
        "idMunicipio": data.idMunicipio,
        "ambito": data.ambito,
        "rolUsuario": data.rolUsuario,
        "versionUsuario": data.versionUsuario,
        "tipoCambioGeografico": null
    }




    const dataRequest = data.infoMenu

    const temporal = () => {

        switch (data.versionUsuario) {
            case 'OC':
                return <ProcessMenu
                    
                    dataRequest={dataRequest}
                    dataResponse={dataResponse}
                    
                />

            case 'JL': // modifica distrito

            dataResponse.infoMenu = dataRequest;
            console.log(dataResponse)
                return <ProcessMenu
                    dataRequest={dataRequest}
                    dataResponse={dataResponse}

                />

            case 'JD': // no modifica nad a
            
                dataResponse.infoMenu = dataRequest;
                return <ProcessMenu
                    dataRequest={dataRequest}
                    dataResponse={dataResponse}

                />

            default:
                break;
        }

    }




    return (
        <Layout style={{ minHeight: "100vh" }} >
            <Header Logo={Heder_Logo} LogoUser={Heder_Logo_User} User={jwtDecode(localStorage.getItem('accessToken')).sub} />
            <Layout >
                {/* <Menu style={{ background: "#F2F2F2" }} /> */}
                <Content style={{ background: "#fff" }} >

                    {temporal()}
                </Content>
            </Layout>

            <Footer className='footer' />
        </Layout>
    );
}
