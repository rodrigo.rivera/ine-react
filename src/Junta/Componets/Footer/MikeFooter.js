import React from "react";
import { Row, Col } from 'antd';
import MikeLadoIzquierdo from '../Footer/MikeLadoIzquierdoFooter'
export default function MikeFooter() {
    return (
        <Row>
            <Col span={8} style={{ textAlign: 'left' }}>{<MikeLadoIzquierdo />}</Col>
            <Col span={8} style={{ textAlign: 'center' }}>Unidad Técnica de Servicios Informáticos</Col>
            <Col span={8} style={{ textAlign: 'right' }}>Versión 1.0/2020</Col>
        </Row>
    );
}
