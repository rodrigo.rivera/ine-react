import React from "react";
import { Row, Col, Layout } from 'antd';
import { Picture } from 'react-responsive-picture';
import logoINE from "../../../assets/img/Footer/logoINE_bco.svg";

export default function MikeLazoIzquierdoFooter() {
    return (
        <Layout>
            <Row>
                <Col span={8}><Picture src={logoINE} /></Col>
                <Col span={8}>© INE México 2020</Col>
                <Col span={8}>CAU</Col>
            </Row>
        </Layout>
    );
}