import React from "react";
import { Layout, Menu, Button, Form } from "antd";
import "./Sider.scss";
import { Link } from "react-router-dom";


export default function Sider(props) {
  const { SubMenu } = Menu;
  const { Sider } = Layout;
  //const [menu] = useState(JSON.parse(props.menu))




  return (

    <Sider collapsible collapsedWidth="0">

      <Menu
        defaultSelectedKeys={["1"]}
        mode="inline"
        style={{ marginTop: "80px", background: "#F2F2F2" }}
      >


        <Link to="/sesiones/aplicaciones">

        </Link>
        {/* Menu 01 */}
        <Form.Item />
        <Form.Item />
        <SubMenu key="sub1" title={<span> <span><strong>Administracion</strong></span> </span>}>
          <Menu.Item key="sub1Item1">Vocales</Menu.Item>
          <Menu.Item key="sub1Item2">Domicilio de Juntas </Menu.Item>


        </SubMenu>

        {/* menu 2 */}
        {/* <SubMenu key="sub2" title={<span> <span>Agenda</span> </span>}>

        </SubMenu> */}
        <Menu.Item key="sub2"> <strong>Agenda</strong>  </Menu.Item>
        {/* menu 3 */}
        <SubMenu key="sub3" title={<span> <span><strong>Sesión</strong></span> </span>}>
          <Menu.Item key="sub3Item1">Convocatoria  </Menu.Item>
          <Menu.Item key="sub3Item2"> Seguimiento </Menu.Item>
          <Menu.Item key="sub3Item3">Acta y Anexos    </Menu.Item>
        </SubMenu>

        {/* menu 4 */}
        {/* <SubMenu key="sub4" title={<span> <span>Reportes</span> </span>}>

        </SubMenu> */}

      </Menu>
      <div align='center' style={{
        paddingTop: '200px'
      }}>
        <Button type="secondary"  >Reportes</Button>
      </div>
    </Sider >

  );
}

