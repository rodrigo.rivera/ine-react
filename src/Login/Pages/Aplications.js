import React from "react";

import Header from "../Componets/Header/Header";
import Footer from "../Componets/Footer/Footer";
import { ReactComponent as Heder_Logo } from "../../assets/img/Header/ID_Docs2.svg";
import { ReactComponent as Heder_Logo_User } from "../../assets/img/Header/user.svg";
import CardsController from "../Componets/Tarjetas/CardsController"


import '../../App.css';
import { Layout } from "antd";
const { Content } = Layout;


export default function Aplications() {
    return (
        <Layout style={{ minHeight: "100vh" }}>

            <Header Logo={Heder_Logo} LogoUser={Heder_Logo_User} User={JSON.parse(localStorage.getItem('username')).user} />
            <Content style={{ paddingTop: '5%', background: "#fff" }}>
                <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" />
                <script src="//<Camaxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
                <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


                <CardsController />

            </Content>
            <Footer />
        </Layout >
    );

}