import React from "react";

//Componentes 
import Header from "../Componets/Header/Header";
import Footer from "../Componets/Footer/Footer";
import Carrusel from "../Componets/Carrusel/Carrusel_nuevo"
import Formulario from "../Componets/Formulario/Formulario"
//Imagnes Auxiliares
import { ReactComponent as Logo_Login } from "../../assets/img/Header/ID_Docs2.svg";
import { ReactComponent as Logo_Header } from "../../assets/img/Header/ID_sesiones.svg";
import 'rc-footer/assets/index.css';
import { Layout } from "antd";
const { Content } = Layout; //Se obtiene el componente hijo de Layout



export default function Home() {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Header Logo={Logo_Header} />
      <Content>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" />
        <script src="//<Camaxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

        <section className="login-block">
          <div className="container">
            <div className="row">
              <div className="banner-sec d-none d-md-block align-self-center col-md-8 " style={{ maxWidth: '100%', paddingRight: '0px', paddingLeft: '0px', paddingTop: '0px', paddingBottom: '0px' }}>
                <Carrusel />
              </div>
              <div className="col-md-4 ">
                <Formulario Logo={Logo_Login} />
              </div>
            </div>
          </div>
        </section>
      </Content>
      <Footer className="footer_login" />
    </Layout>

  );

}
