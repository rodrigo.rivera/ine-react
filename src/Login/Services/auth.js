//import { basePath, apiVersion } from "./config";
//import { ACCESS_TOKEN, REFRESH_TOKEN } from "./constanst";
import jwtDecode from "jwt-decode";


/**
 * Obtiene el token que se almacena en el local storage
 */
export function getAccessTokenApi() {
  const accessToken = localStorage.getItem("accessToken");//Se verifica que el token exista en el localstorage

  if (!accessToken || accessToken === "null") {
    return null;
  }

  return willExpireToken(accessToken) ? null : accessToken;
}


/**
 * Verifa el token de refresToken para tambien actualizarlo(Ejemplo)
 */
export function getRefreshTokenApi() {
  const refreshToken = localStorage.getItem("refreshToken");

  if (!refreshToken || refreshToken === "null") {
    return null;
  }

  return willExpireToken(refreshToken) ? null : refreshToken;
}

/**
 *  vuelve a solicitar el token 
 * @param {*} refreshToken 
 */
export function refreshAccessTokenApi(refreshToken) {
  /*const url = `${basePath}/${apiVersion}/refresh-access-token`;
  const bodyObj = {
    refreshToken: refreshToken
  };
  const params = {
    method: "POST",
    body: JSON.stringify(bodyObj),
    headers: {
      "Content-Type": "application/json"
    }
  };

  fetch(url, params)
    .then(response => {
      if (response.status !== 200) {
        return null;
      }
      return response.json();
    })
    .then(result => {
      if (!result) {
        logout();
      } else {
        const { accessToken, refreshToken } = result;
        localStorage.setItem(ACCESS_TOKEN, accessToken);
        localStorage.setItem(REFRESH_TOKEN, refreshToken);
      }
    });*/
}



/**
 * Elimina el token del navegador
 */
export function logout() {
  localStorage.removeItem("accessToken");
  localStorage.removeItem("refreshToken");
}

/**
 * Verifica que no haya expirado el token 
 * @param {*} token 
 */
function willExpireToken(token) {
  const seconds = 60;
  const metaToken = jwtDecode(token);//decodifica el token 
  const { exp } = metaToken;//Se obtiene el tiempo de vida del token
  const now = (Date.now() + seconds) / 1000;
  return now > exp;//verifica que no hay expirado
}
