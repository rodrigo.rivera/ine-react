import React, { useState } from "react";
import { Layout, Menu, Icon } from "antd";


import "./Sider.scss";

export default function Home(props) {
  const { SubMenu } = Menu;
  const { Sider } = Layout;
  const [menu] = useState(JSON.parse(props.menu))




  return (

    <Sider collapsible style={{ background: "#fff" }} width={230}>
      <Menu
        defaultSelectedKeys={["1"]}
        mode="inline"
        style={{ marginTop: "50px" }}
      >
        {/* Menu 01 */}
        {(menu["0"] !== undefined && menu["0"].idMenu) === 1 &&
          <SubMenu key="sub1" title={<span> <Icon type="mail" /><span>{menu["0"].nombreMenu}</span> </span>}>
            <SubMenu key="sub1sub1" title={menu["0"].subMenus["0"].nombreSubMenu}>
              <Menu.Item key="sub1sub1Item1">{menu["0"].subMenus["0"].modulos["0"].nombreModulo}</Menu.Item>
              <Menu.Item key="sub1sub1Item2">{menu["0"].subMenus["0"].modulos["1"].nombreModulo}</Menu.Item>
              <Menu.Item key="sub1sub1Item3">{menu["0"].subMenus["0"].modulos["2"].nombreModulo}</Menu.Item>
            </SubMenu>
            {/* {console.log(menu["1"].subMenus["0"].nombreSubMenu)} */}
            {/* <SubMenu key="sub1sub2" title={menu["1"].subMenus["0"].nombreSubMenu}>
              <Menu.Item key="2">{menu["1"].subMenus["0"].modulos["0"].nombreModulo}</Menu.Item>
            </SubMenu>
            <SubMenu key="sub1sub3" title={menu["2"].subMenus["0"].nombreSubMenu}>
              <Menu.Item key="3">{menu["2"].subMenus["0"].modulos["0"].nombreModulo}</Menu.Item>
            </SubMenu> */}
          </SubMenu>
        }
        {/* menu 2 */}

        {(menu["1"] !== undefined && menu["1"].idMenu === 2) &&
          <SubMenu key="sub2" title={<span>    <Icon type="desktop" /> <span>{menu["1"].nombreMenu} </span> </span>}>
            {(menu["1"].subMenus["0"] !== undefined && menu["1"].subMenus["0"].idSubMenu) === 1 &&
              <SubMenu key="sub2sub1" title={menu["1"].subMenus["0"].nombreSubMenu}>
                <Menu.Item key="sub2sub1-1">{menu["1"].subMenus["0"].modulos["0"].nombreModulo}</Menu.Item>
                <Menu.Item key="sub2sub1-2">{menu["1"].subMenus["0"].modulos["1"].nombreModulo}</Menu.Item>
                <Menu.Item key="sub2sub1-3">{menu["1"].subMenus["0"].modulos["2"].nombreModulo}</Menu.Item>
                <Menu.Item key="sub2sub1-4">{menu["1"].subMenus["0"].modulos["3"].nombreModulo}</Menu.Item>





                {/* {(menu["1"].subMenus["0"].modulos["0"] !== undefined && menu["1"].subMenus["0"].modulos["0"].idModulo === 2) &&
                } */}
                {/* {(menu["1"].subMenus["0"].modulos["1"] !== undefined && menu["1"].subMenus["0"].modulos["1"].idModulo === 3) &&
                  <Menu.Item key="5">{menu["1"].subMenus["0"].modulos["1"].nombreModulo}</Menu.Item>
                } */}
              </SubMenu>
            }
            {/* {(menu["0"].subMenus["1"] !== undefined && menu["0"].subMenus["1"].idSubMenu === 1) &&
              <SubMenu key="sub2sub2" title="Sedes">
                {(menu["1"].subMenus["1"].modulos["0"] !== undefined && menu["1"].subMenus["1"].modulos["0"].idModulo === 4) &&
                  <Menu.Item key="6">{menu["1"].subMenus["1"].modulos["0"].nombreModulo}</Menu.Item>
                }
                {(menu["1"].subMenus["1"].modulos["0"] !== undefined && menu["1"].subMenus["1"].modulos["0"].idModulo === 5) &&
                  <Menu.Item key="7">{menu["1"].subMenus["1"].modulos["0"].nombreModulo}</Menu.Item>
                }
              </SubMenu>
            } */}
          </SubMenu>
        }
        {/* MENU 3 */}
        {(menu["2"] !== undefined && menu["2"].idMenu === 3) &&
          <SubMenu key="sub3" title={<span> <Icon type="user" /><span> {menu["2"].nombreMenu}</span></span>}>
            <Menu.Item key="sub3-1">{menu["2"].subMenus["0"].modulos["0"].nombreModulo}</Menu.Item>
            <Menu.Item key="sub3-2">{menu["2"].subMenus["0"].modulos["1"].nombreModulo}</Menu.Item>

            {/* {(menu["2"].subMenus["0"] !== undefined && menu["2"].subMenus["0"].idSubMenu === 1) &&
              <SubMenu key="sub3sub1" title={menu["2"].subMenus["0"].nombreSubMenu}>
                {(menu["2"].subMenus["0"].modulos["0"] !== undefined && menu["2"].subMenus["0"].modulos["0"].idModulo === 9) &&
                  <Menu.Item key="8">{menu["2"].subMenus["0"].modulos["0"].nombreModulo}</Menu.Item>
                }
                {(menu["2"].subMenus["0"].modulos["1"] !== undefined && menu["2"].subMenus["0"].modulos["1"].idModulo === 10) &&
                  <Menu.Item key="9">{menu["2"].subMenus["0"].modulos["1"].nombreModulo}</Menu.Item>
                }
                {(menu["2"].subMenus["0"].modulos["2"] !== undefined && menu["2"].subMenus["0"].modulos["2"].idModulo === 22) &&
                  <Menu.Item key="10">{menu["2"].subMenus["0"].modulos["2"].nombreModulo}</Menu.Item>
                }
              </SubMenu>
            }
            {(menu["2"].subMenus["1"] !== undefined && menu["2"].subMenus["1"].idSubMenu === 2) &&
              <SubMenu key="sub4" title="Intercambios">
                {(menu["2"].subMenus["1"].modulos["0"] !== undefined && menu["2"].subMenus["1"].modulos["0"].idModulo === 18) &&
                  <Menu.Item key="11">{menu["2"].subMenus["1"].modulos["0"].nombreModulo}</Menu.Item>
                }
                {(menu["2"].subMenus["1"].modulos["1"] !== undefined && menu["2"].subMenus["1"].modulos["1"].idModulo === 19) &&
                  <Menu.Item key="12">{menu["2"].subMenus["1"].modulos["0"].nombreModulo}</Menu.Item>
                }
                {(menu["2"].subMenus["1"].modulos["2"] !== undefined && menu["2"].subMenus["1"].modulos["2"].idModulo === 20) &&
                  <Menu.Item key="13">{menu["2"].subMenus["1"].modulos["2"].nombreModulo}</Menu.Item>
                }
              </SubMenu>
            } */}
          </SubMenu>
        }

        {/* MENU 8 */}
        {(menu["3"] !== undefined && menu["3"].idMenu === 8) &&
          <SubMenu key="sub4" title={<span> <Icon type="user" /><span> {menu["3"].nombreMenu}</span></span>}>
            <Menu.Item key="sub4-1">{menu["3"].subMenus["0"].modulos["0"].nombreModulo}</Menu.Item>
            <Menu.Item key="sub4-2">{menu["3"].subMenus["0"].modulos["1"].nombreModulo}</Menu.Item>

            {/* {(menu["3"].subMenus["0"] !== undefined && menu["3"].subMenus["0"].idSubMenu === 1) &&
              <SubMenu key="sub4sub1" title={menu["3"].subMenus["0"].nombreSubMenu}>
                {(menu["3"].subMenus["0"].modulos["0"] !== undefined && menu["3"].subMenus["0"].modulos["0"].idModulo === 9) &&
                  <Menu.Item key="8">{menu["3"].subMenus["0"].modulos["0"].nombreModulo}</Menu.Item>
                }
                {(menu["3"].subMenus["0"].modulos["1"] !== undefined && menu["3"].subMenus["0"].modulos["1"].idModulo === 10) &&
                  <Menu.Item key="9">{menu["3"].subMenus["0"].modulos["1"].nombreModulo}</Menu.Item>
                }
                {(menu["3"].subMenus["0"].modulos["2"] !== undefined && menu["3"].subMenus["0"].modulos["2"].idModulo === 22) &&
                  <Menu.Item key="10">{menu["3"].subMenus["0"].modulos["2"].nombreModulo}</Menu.Item>
                }
              </SubMenu>
            }
            {(menu["3"].subMenus["1"] !== undefined && menu["3"].subMenus["1"].idSubMenu === 2) &&
              <SubMenu key="sub4" title="Intercambios">
                {(menu["3"].subMenus["1"].modulos["0"] !== undefined && menu["3"].subMenus["1"].modulos["0"].idModulo === 18) &&
                  <Menu.Item key="11">{menu["3"].subMenus["1"].modulos["0"].nombreModulo}</Menu.Item>
                }
                {(menu["3"].subMenus["1"].modulos["1"] !== undefined && menu["3"].subMenus["1"].modulos["1"].idModulo === 19) &&
                  <Menu.Item key="12">{menu["3"].subMenus["1"].modulos["0"].nombreModulo}</Menu.Item>
                }
                {(menu["3"].subMenus["1"].modulos["2"] !== undefined && menu["3"].subMenus["1"].modulos["2"].idModulo === 20) &&
                  <Menu.Item key="13">{menu["3"].subMenus["1"].modulos["2"].nombreModulo}</Menu.Item>
                }
              </SubMenu>
            } */}
          </SubMenu>
        }
      </Menu>
    </Sider>
  );
}
