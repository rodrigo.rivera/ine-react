import React from "react";
import MikeLadoIzquierdo from '../Footer/MikeLadoIzquierdoFooter'
import { Layout, Col, Row } from "antd";
const { Footer } = Layout;
export default function MikeFooter() {
    return (
        <Footer>
            <Row style={{ color: 'white' }} >
                <Col span={8} style={{ textAlign: 'left' }}>{<MikeLadoIzquierdo />}</Col>
                <Col span={8} style={{ textAlign: 'center' }}>Unidad Técnica de Servicios Informáticos</Col>
                <Col span={8} style={{ textAlign: 'right' }}>Versión 1.0/2020</Col>
            </ Row>
        </Footer>
    );
}
