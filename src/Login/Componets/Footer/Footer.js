import React from "react";
import { Layout } from "antd";
import "./Footer.scss";
import logoINE from "../../../assets/img/Footer/logoINE_bco.svg";

export default function Footer(props) {

  const { Footer } = Layout;

  return (
    <Footer className="footer">
      <div className="footer__instituto">
        <div className="d-md-none">
          <img src={logoINE} alt="Logo" className="center" />
        </div>
        <div className=" d-none d-md-block">
          <img src={logoINE} alt="Logo" />
          <span dangerouslySetInnerHTML={{ __html: "&copy;" }} />
          INE México {props.anio}
          <a href="https://cau.ine.mx/" target="blank">
            CAU
            </a>
          <a href="https://centralelectoral.ine.mx/2019/03/20/traves-inetel-puedes-reportar-robo-extravio-credencial-votar/" target="blank">
            INEtel
            </a>
        </div>
      </div>
      <div className=" d-none d-md-block footer__area">
        Unidad Técnica de Servicios Informáticos
      </div>
      <div className="d-none d-md-block footer__version">
        <span >{"1.0.0 Rev.1 20/02/2020 16:00"}</span> &nbsp;&nbsp;&nbsp;&nbsp; {props.version} / {new Date().getFullYear()}
      </div>
    </Footer>
  );

}
