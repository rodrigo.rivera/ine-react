import React from "react";
import { Row, Col, Layout } from 'antd';
import { Picture } from 'react-responsive-picture';
import logoINE from "../../../assets/img/Footer/logoINE_bco.svg";
import "./Footer.scss";

export default function MikeLazoIzquierdoFooter() {
    return (
        <Layout >
            <Row>
                <Col span={6}><Picture src={logoINE} /></Col>
                <Col span={6}>© INE México 2020</Col>
                <Col span={6}>CAU</Col>
                <Col span={6}>INEtel</Col>
            </Row>
        </Layout>
    );
}