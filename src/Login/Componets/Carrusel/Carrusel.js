import React from "react";
import images from "../../Config/Carrusel";
import { Carousel } from "antd";
import './Carrusel.scss';

export default function Carrusel() {
  var settings = {
    dots: true,
    infinite: true,
    speed: 8000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay:true,
    
  };
  return (
    <Carousel autoplay speed="8000"  className="carrusel" >
      {images.map(img => (
        <img key={img.id} src={img.url} alt="" />
      ))}
    </Carousel>
  );
}

