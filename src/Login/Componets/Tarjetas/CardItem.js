import React from 'react'
import 'antd/dist/antd.css'

import axios from 'axios';
import { basePath } from "../../../Config/Server";

export default function CardItem(props) {

    const { idSistema, urlImg, rutaDeApp } = props
    const Login = (e) => {

        //e.preventDefault();
        let tempUser = JSON.parse(localStorage.getItem("username"));
        const data = {
            "usuario": tempUser.user, //"armando.calleja",
            "password": tempUser.pass, //"luapfeVxf+UvOjMJ9xRZjA=="
            "versionAplicacion": "1",
            "idSistema": idSistema
        }
        localStorage.setItem("idSistema", idSistema);
        const url = `${basePath}/ingresaSistema`
        axios.post(url, data).then(response => {
            if (response.status === 200) {
                console.log(response.data.entity)
                if (response.data.entity.code === 200) {
                    //localStorage.setItem("data", JSON.stringify(response.data.entity.datosUsuario.infoMenu));
                    localStorage.setItem("data", JSON.stringify(response.data.entity.datosUsuario));
                    localStorage.setItem("accessToken", response.data.entity.datosUsuario.tknA);
                    //console.log(response.data.entity.datosUsuario)
                    localStorage.setItem("menu",
                        JSON.stringify(response.data.entity.datosUsuario.infoMenu.listMenu)
                    );
                    window.location.href = rutaDeApp; //Se redirecciona al home

                } else {
                    alert(response.data.entity.message)
                }
            }
        }).catch(err => {
            console.log("Erorr !", err)
            alert("ocurrio un error ! ")
        })
    };


    return (
        <div className="polaroid" onClick={Login} >
            <div className="card" >
                <img className="card-img-top" src={urlImg} alt="aplicaciones" />
                <div className="card-body">
                    {/* <p className="card-text">TEsting cards text to build on the card title and make up the bulk of the card's content.</p> */}
                </div>
            </div>
        </div >


    )

}
//export default CardItem|

