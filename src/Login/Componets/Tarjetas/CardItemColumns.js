import { Card, Col, Row } from 'antd';
import React from 'react';
import CardItem from './CardItem';
const { Meta } = Card;

function CardItemColumns() {

    return (
        <div style={{ background: '#ECECEC', padding: '30px' }}>
            <Row gutter={16}>
                <Col span={8}>
                    <CardItem />
                </Col>
                <Col span={8}>
                    <Card title="Card title" bordered={false}>
                        Card content
        </Card>
                </Col>
                <Col span={8}>
                    <Card title="Card title" bordered={false}>
                        Card content
        </Card>
                </Col>
            </Row>
        </div>
    )
}
export default CardItemColumns