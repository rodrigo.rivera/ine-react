import React, { Component } from 'react'
import CardItem from './CardItem'
import 'antd/dist/antd.css'
import consejo from '../Tarjetas/Consejo_carta.svg'
import junta from '../Tarjetas/Junta_carta.svg'
import consejoMobile from '../Tarjetas/Consejo_01.svg'
import juntaMobile from '../Tarjetas/Juntas_01.svg'
import Container from '@material-ui/core/Container';

function iteraEnSistemas(sistemasHabilitados, className) {
    var imageConsejo = className === "col d-none d-md-block" ? consejo : consejoMobile
    var imageJunta = className === "col d-none d-md-block" ? junta : juntaMobile


    return (
        sistemasHabilitados.map(app => (
            <div key={app.idSistema} className={className}>
                <CardItem
                    idSistema={app.idSistema}
                    cardData={app}
                    nombreCortoSistema={app.nombreCortoSistema}
                    nombreSistema={app.nombreSistema}

                    urlImg={app.idSistema === 203 ? imageConsejo : imageJunta}
                    rutaDeApp={app.idSistema === 203 ? "/sesiones/consejo" : "/sesiones/junta"}
                />
            </div>
        ))
    )
}
export class CardsController extends Component {
    //Lista de aplicaciones disponibles
    apps = JSON.parse(localStorage.getItem('permisoSistemas'))

    render() {
        //console.log(this.apps)
        const sistemasHabilitados = this.apps.filter(function (item) {
            return (item.idSistema === 2 | item.idSistema === 203)
        })
        if (sistemasHabilitados.length === 1) {
            return (
                <Container maxWidth="sm">
                    <div className="container shadow-none p-3 mb-5 bg-white rounded">
                        <div className="row ">
                            {
                                iteraEnSistemas(sistemasHabilitados, "col d-none d-md-block")
                            }
                            {
                                iteraEnSistemas(sistemasHabilitados, "col-md-4 d-md-none")
                            }
                        </div>
                    </div>
                </Container>
            )
        }
        else {
            return (
                <div className="container shadow-none p-3 mb-5 bg-white rounded">
                    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossOrigin="anonymous"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossOrigin="anonymous"></script>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossOrigin="anonymous" />
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossOrigin="anonymous"></script>
                    <div className="row ">
                        {
                            iteraEnSistemas(sistemasHabilitados, "col d-none d-md-block")
                        }
                        {
                            iteraEnSistemas(sistemasHabilitados, "col-md-4 d-md-none")
                        }
                    </div>
                </div >
            )
        }
    }
}

export default CardsController
