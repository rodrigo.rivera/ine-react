import React from "react";
import "./Proceso_Menu.scss";
//import data from "../../pruebas/data.json";
import { Popover, Select, Icon } from "antd";
import useAuth from "../../hooks/useAuth";
import axios from "axios";
import { infoMenu } from "../../api/config";

function handleChange(value) {
  console.log("Se eligió un valor");
  console.log(`selected ${value}`);
  const url = `${infoMenu}`;
  /*axios.post(url, inputs).then(response => {
    const { code, message } = response.data.entity; 
  });*/
}

const DATOS = [
  {
    "descripcion": "PEL-ORD-COAHUILA",
    "idProcesoElectoral": "14"
  },
  {
    "descripcion": "PEL-ORD-OAXACA",
    "idProcesoElectoral": "15"
  }
];
var comboEstado = true;
var comboDistrito = true;
//const {  } = Select; 
var procesosElectorales = new Set();
const { Option } = Select;
DATOS.forEach(dato => {
  procesosElectorales.add(dato.descripcion);
  //console.log("***procesosElectorales: ",procesosElectorales);
  //idProcesoElectoral.add(dato.Hijo);
});
//console.log("-----procesosElectorales: ", procesosElectorales);

var contenidoMenuUsuario = (versionUsuario, listaDetalles) => {
  //const procesosElectorales = new Set();
  const estado = new Set();
  const ditrito = new Set();
  //const {listaDetalles} = listaDetallesTodo;                                       
  console.log("----- listaDetalles: ", listaDetalles);
  //console.log("----- listaDetalles: ", listaDetalles);
  //for (let item of listaDetalles) console.log("----- listaDetalles: ", item)
  /**listaDetalles.forEach(detalle => {
          procesosElectorales.add(detalle.descripcion)
        }); 
  console.log('Proceso Electoral')
  for (let item of procesosElectorales) console.log(item)
  console.log("listaDetalles: ", listaDetalles);*/
  return (
    <div>
      {

        versionUsuario === "OC" ? comboEstado = true : comboEstado = true
      }
      <div className="etiqueta">Proceso Electoral</div>
      <Select
        defaultValue="Selecciona"
        style={{ width: 280 }}
        onChange={handleChange}
        value={listaDetalles}
      >

      </Select>
      <p> </p>
      <div className="etiqueta">Entidad</div>

      <Select defaultValue="Selecciona" style={{ width: 280 }} disabled={comboEstado}>
        <Option value="BajaCaliforniaSur">Baja California Sur</Option>
        <Option value="CDMX">Ciudad de México</Option>
        <Option value="EstadoMexico">Estado de México</Option>
        <Option value="Yucatan">San Luis Potosí</Option>
      </Select>
      <p> </p>
      <div className="etiqueta">Distrito</div>

      <Select defaultValue="Selecciona" style={{ width: 280 }} disabled={comboDistrito}>
        <Option value="Tanga">Tangamandapio</Option>
        <Option value="Paran">Parangaricutiro</Option>
      </Select>
    </div>
  );

}

export default function Header(props) {
  const { tipo_eleccion_gen, tipo_eleccion_esp } = props;
  //const { usuario, isLoading } = useAuth();
  //console.log("Proceso_Menu-props: ",props);
  //console.log("Proceso_Menu-user: ",user); 
  const { user, isLoading, estado, distrito, idDistrito, versionUsuario, listaDetalles } = useAuth();
  return (
    <Popover content={contenidoMenuUsuario(versionUsuario, listaDetalles)} trigger="click">

      <p className="proceso">
        {tipo_eleccion_gen}
        <br />
        {tipo_eleccion_esp &&
          <>
            <Icon type="environment" theme="filled" /> {tipo_eleccion_esp}
          </>
        }
      </p>

    </Popover>
  );
}
