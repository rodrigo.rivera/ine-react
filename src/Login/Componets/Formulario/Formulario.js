import React, { useState } from "react";
import { Form, Icon, Input, notification } from "antd";
import Container from '@material-ui/core/Container';
import Recaptcha from "react-recaptcha";
import "./Login.scss";
import "../../../../src/style/custom-antd.css"
import "../../../Login/Componets/Formulario/Login.scss"
import axios from "axios";
import { basePath } from "../../../Config/Server";
import { Button } from 'antd';

import { CloseCircleOutlined } from '@ant-design/icons';


export default function Formulario(props) {
  const CAP_KEY = "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI";
  const { Logo } = props; //Se recibe el logo por props  
  const [capcha, setCapcha] = useState(false);
  const [inputs, setInputs] = useState({
    usuario: "",
    password: "",

  });

  const verifyCallback = response => {
    setCapcha({ capcha }); //función que comprueba que el recapcha se selecciono correctamente
  };
  const onloadCallback = () => {
    console.log("captcha successfully loaded"); //Realiza un callback al capcha
  };
  //Funcíon que obtiene los cambios que a tenido el formulario
  const changeForm = e => {

    var regex = /^([\w\d.\-_@])+$/
    var str = e.target.value
    var result = regex.test(str)
    if (!result) {
      showAlert(7, "Nombre de usuario inválido.")
      console.log(escapeRegExp(str))
    }
    else {
      setInputs({
        ...inputs,
        [e.target.name]: escapeRegExp(e.target.value)
      });
    }

  };
  function escapeRegExp(cadena) {
    return cadena.replace(/([^\w\d.\-_@])+$/, ''); // $& significa la totalidad de la cadena coincidente
  }
  const key = 'updatable';
  function showAlert(code, message) {
    //No se logró autenticar. Datos de ingreso--- incorrectos
    var width = message.length > 40 ? message.length * 10.23 : 400
    notification.open({
      key,
      className: 'ant-notification-notice',
      message: message,
      placement: 'topLeft',
      duration: 2,
      style: {
        verticalAlign: 'middle', background: '#ffeae6',
        width: width + 'px', height: '56px', borderColor: '#79134c',
        borderWidth: '1px', borderStyle: 'solid',
        font: 'Roboto', fontSize: '14px'
      },
      icon: <CloseCircleOutlined style={{ verticalAlign: 'middle', color: '#79134c' }} />,
    });
  }
  const login = e => {
    e.preventDefault();
    if (!capcha) {
      // if (false) {
      //Se verifica el capcha
      //alert("Selecciona el capcha");
      showAlert(7, "Selecciona el captcha")

    } else {
      //Se realiza la petición al server
      const url = `${basePath}/validateUser`;
      console.log(url);
      //Se realiza la peticion

      axios.post(url,
        inputs
        // {
        //   "usuario": "armando.calleja", // oc
        //   //"usuario": "desa13-05", // JD
        //   //"usuario": "desa13-06", // JD
        //   //"usuario": "desa13-00", // JL
        //   "password": "password",
        // }
      ).then(response => {
        const { code, message } = response.data.entity;
        if (code === 400 || code === 500) {
          //alert(message);
          console.log("eroro " + message)

          //message.error(message, 3);
          showAlert(code, message)
        } else {
          console.log("hola")
          if (code === 200) {
            console.log("datosUsuario", response.data.entity.datosUsuario);

            localStorage.setItem("permisoSistemas", JSON.stringify(response.data.entity.datosUsuario.permisoSistemas))
            localStorage.setItem("username", JSON.stringify({
              user: response.data.entity.datosUsuario.username,
              pass: response.data.entity.datosUsuario.claveCifrada
            })
            )
            //console.log(response.data.entity.datosUsuario.username)

            window.location.href = "/sesiones/aplicaciones"; //Se redirecciona al home
          } else {
            console.log("hay error")
          }
        }
      }).catch(error => {
        console.log("ocurrio un error ", error)
        //message.error('Error al intentrar acceder a el servidor', 7);
        //alert("Error al intentrar acceder a el servidor ")
        showAlert(7, "Error al intentrar acceder a el servidor. Revisa tu conexión a internet o contacta al administrador. ")
      });
    }
  };

  return (
    <Container fixed >
      <Form.Item className="d-none d-md-block"></Form.Item>
      <Logo />
      <Container fixed  >
        <Form className="login-form" onChange={changeForm} onSubmit={login} >
          <Form.Item className="d-none d-md-block" />

          <Form.Item label="Usuario">
            <Input
              size="large"
              prefix={<Icon type="user" style={{ color: "#d5007f" }} />}
              placeholder="Usuario"
              name="usuario"
              style={{ width: "100%", background: "#FFFFFF" }}
              required={true}
              tabIndex='1'
              maxLength='50'
            />
          </Form.Item>
          <Form.Item label="Contraseña">
            <Input.Password
              size="large"
              prefix={<Icon type="lock" style={{ color: "#d5007f" }} />}
              type="password"
              name="password"
              placeholder="Contraseña"
              style={{ width: "100%", background: "#FFFFFF" }}
              tabIndex='2'
              required={true}
              maxLength='50'
            //style={{ width: '100%', backgroundColor: '#FFFFFF' }
            //style={{ backgroundColor: "#d5007f" }}
            />
          </Form.Item>
          <Form.Item className="captcha__capcha">
            <div>
              <Recaptcha
                onloadCallback={onloadCallback}
                verifyCallback={verifyCallback}
                sitekey={CAP_KEY}
                hl={"es"}
                render="explicit"
                tabIndex='3'
              //size="normal"
              // style=".g-recaptcha {
              //   transform:scale(0.77);
              //   -webkit-transform:scal(0.77);
              //   transform-origin:0 0;
              //   -webkit-transform-origin:0 0;
              //   } "
              />
            </div>
          </Form.Item>
          <Form.Item className="d-none d-md-block" />
          <Form.Item>
            <Button block type="primary" htmlType="submit"
              className="login-form-button" tabIndex='4'>
              Iniciar Sesión
          </Button>

          </Form.Item>
          <div style={{ textAlign: 'center' }}>
            <a style={{ color: '#D5007F', textDecoration: 'underline' }}
              href="https://cua.ine.mx/claveacceso/" tabIndex='5'>Cambiar Contraseña</a>

          </div>
          <div style={{ textAlign: 'center' }}>
            <a style={{ color: '#D5007F', textDecoration: 'underline' }} tabIndex='6'
              href="html">Protección de datos</a>

          </div>
          <Form.Item />
          <Form.Item />
        </Form >
      </Container>
    </Container>
  );

}

