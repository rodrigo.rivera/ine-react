import React from "react";
import Header from "../Componets/Header/Header";
import Footer from "../Componets/Footer/Footer";
import Menu from "../Componets/Sider/Sider";
import { ReactComponent as Heder_Logo } from "../Assets/img/Logo.svg";
import { ReactComponent as Heder_Logo_User } from "../Assets/img/user.svg";
import jwtDecode from "jwt-decode";
import { Layout } from "antd";
import ProcessMenu from "../Componets/ProcessMenu/ProcessMenu";
const { Content } = Layout;

export default function Home() {



    const data = JSON.parse(localStorage.getItem("data"));
    //console.log(data)

    const dataResponse = {
        "infoMenu": {
            "detalleSelec": null,
            "listaDetalles": null,
            "estadoSelec": null,
            "listaEstados": null,
            "distritoFedSelec": null,
            "listaDistritosFed": null,
            "distritoLocSelec": null,
            "listaDistritosLoc": null,
            "listaDistritosFedAux": null,
            "municipioSelec": null,
            "listaMunicipios": null,
            "listMenu": null,
        },
        "idSistema": data.idSistema,
        "idEstado": data.idEstado,
        "idDistrito": data.idDistrito,
        "idMunicipio": data.idMunicipio,
        "ambito": data.ambito,
        "rolUsuario": data.rolUsuario,
        "versionUsuario": data.versionUsuario,
        "tipoCambioGeografico": null
    }

    const dataRequest = data.infoMenu
    const temporal = () => {

        dataResponse.infoMenu = dataRequest;
        return <ProcessMenu
            dataRequest={dataRequest}
            dataResponse={dataResponse}
        />

    }

    return (
        <Layout style={{ minHeight: "100vh" }}>
            <Header Logo={Heder_Logo} LogoUser={Heder_Logo_User} User={jwtDecode(localStorage.getItem('accessToken')).sub} />

            <Layout>
                {/* <Sider><Menu /></Sider> */}
                <Menu style={{ background: "#F2F2F2" }} />
                <Content style={{ background: "#fff" }} >
                    {temporal()}
                </Content>
            </Layout>

            <Footer className='footer' />
        </Layout>
    );
}