import React from "react";
import { Layout, Button } from "antd"; //Se importan los componentes de ant desig
import LogoUser from "../../../assets/img/Header/user.svg";
import "./Header.scss";
import { logout } from "../../../Router/Auth";
import { Dialog, DialogContent, DialogActions } from "@material-ui/core";


export default function Header(props) {
  const { Logo, User } = props; //Se recibe el logo por props
  const { Header } = Layout; //Se importa el header del componente del Layout
  const [alertOpen, setAletOpen] = React.useState(false);

  const handleClickOpen = () => {
    setAletOpen(true);
  };

  const handleClose = () => {
    setAletOpen(false);
  };
  return (
    <Header>
      <div className="menu-top">
        {/*Se crea la estructura del header  */}
        <div className="menu-top__logo">
          <Logo />
        </div>
        <div className="menu-top__ayuda">
          <p>

            {User &&
              <>
                <img src={LogoUser} className="user" alt="user" />
                {User} |
                <Button style={{ color: 'white' }} type="link" onClick={handleClickOpen}>
                  Cerrar sesión
                </Button>
                <Dialog
                  open={alertOpen}
                >
                  <DialogContent>
                    Deseas Cerrar Sesión
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={logout}>
                      Aceptar
                    </Button>
                    <Button onClick={handleClose}>
                      Cancelar
                    </Button>
                  </DialogActions>


                </Dialog>
              </>
            }
          </p>
        </div>
      </div>
    </Header>
  );
}
