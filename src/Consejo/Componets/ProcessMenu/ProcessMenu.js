import React, { useState } from 'react'
import axios from 'axios';
import "./Proceso_Menu.scss";
import { Popover, Select, Icon } from "antd";
import { basePath } from "../../../Config/Server"

const { Option } = Select;


export default function ProcessMenu(props) {
    const url = `${basePath}/cambiaInfoMenu`

    const {
        dataResponse, // se envia a el servicio 
    } = props;


    /**
     *  Regres el distrito seleccionado 
     */
    const getDistritoProp = () => {

        if (dataResponse.infoMenu.distritoFedSelec != null) {
            return dataResponse.infoMenu.distritoFedSelec;
        }
        if (dataResponse.infoMenu.distritoLocSelec != null) {
            return dataResponse.infoMenu.distritoLocSelec;
        }
        if (dataResponse.infoMenu.municipioSelec != null) {
            var temp = { nombreDistrito: dataResponse.infoMenu.municipioSelec }
            return temp;
        }
        return null;
    }


    /**
     * Regresa la lista de los distritos 
     */
    const getListaDistrito = () => {
        if (dataResponse.infoMenu.listaDistritosFed != null) {
            return dataResponse.infoMenu.listaDistritosFed;
        }
        if (dataResponse.infoMenu.listaDistritosLoc != null) {
            return dataResponse.infoMenu.listaDistritosLoc;
        }
        if (dataResponse.infoMenu.listaMunicipios != null) {
            return dataResponse.infoMenu.listaMunicipios;
        }
        return null;
    }


    /**
     * Construlle el segundo mensaje  de popover
     */
    const getMessagePopover = () => {
        if (dataResponse.infoMenu.detalleSelec == null) {
            return "Oficinas Centrales"
        }
        if (dataResponse.infoMenu.distritoFedSelec != null) {
            return `${dataResponse.infoMenu.estadoSelec.nombreEstado} - ${dataResponse.infoMenu.distritoFedSelec.nombreDistrito}`;
        }
        if (dataResponse.infoMenu.distritoLocSelec != null) {
            return dataResponse.infoMenu.listaDistritosLoc.nombreDistritoLocal;
        }
        if (dataResponse.infoMenu.municipioSelec != null) {
            return dataResponse.infoMenu.listaMunicipios.nombreMunicipio;
        }

        return dataResponse.infoMenu.estadoSelec.nombreEstado;
    }
    /**
     * Estados 
     */
    const [menu, setMenu] = useState(null)
    const [proceso, setProceso] = useState(dataResponse.infoMenu.detalleSelec);
    const [entidad, setEntidad] = useState(dataResponse.infoMenu.estadoSelec);
    const [distrito, setDistrito] = useState(getDistritoProp);
    const [selectEntidad, setSelectEntidad] = useState()
    const [selectDistrito, setSelectDistrito] = useState(getListaDistrito);
    const [popoverSegundo, setPopoverSegundo] = useState(getMessagePopover)


    /**
     * Cambia el proceso 
     * @param {*} value 
     */
    const changeProceso = value => {
        //console.log("Cambiaste el proceso " + value);        
        const tempProceso = dataResponse.infoMenu.listaDetalles[value];
        setProceso(tempProceso);
        //console.log("antes---- selectEntidad: ", entidad, "selectDistrito: ", distrito)
        setDatos()
        dataResponse.infoMenu.detalleSelec = tempProceso;
        dataResponse.tipoCambioGeografico = 1;
        axios.post(url, dataResponse).then(response => {
            // console.log(response)            
            setSelectEntidad(response.data.infoMenu.listaEstados)

            response.data.infoMenu.menu = ! null ? setMenu(response.data.infoMenu) : console.log()
        }).catch(err => {
            console.log("Error proceso ! " + err);
        });

    }

    /**
     * Cambia la entiadad 
     * @param {*} value 
     */
    const ChangeEndidad = value => {
        //console.log("Cambiaste el Entidad " , value);
        const tempEntidad = selectEntidad[value];
        //console.log(tempEntidad);listaMunicipios
        setEntidad(tempEntidad)
        setPopoverSegundo(tempEntidad.nombreEstado)
        dataResponse.infoMenu.estadoSelec = tempEntidad;
        dataResponse.tipoCambioGeografico = 2;
        //console.log(dataResponse)
        axios.post(url, dataResponse).then(response => {
            //console.log("exito Entidad ! ", response)
            if (response.data.infoMenu.listaDistritosFed != null) {
                dataResponse.infoMenu.listaDistritosFed = response.data.infoMenu.listaDistritosFed;
                setSelectDistrito(response.data.infoMenu.listaDistritosFed);
            }
            if (response.data.infoMenu.listaDistritosLoc != null) {
                dataResponse.infoMenu.listaDistritosLoc = response.data.infoMenu.listaDistritosLoc;
                setSelectDistrito(response.data.infoMenu.listaDistritosLoc);
            }
            if (response.data.infoMenu.listaMunicipios != null) {
                dataResponse.infoMenu.listaMunicipios = response.data.infoMenu.listaMunicipios;
                setSelectDistrito(response.data.infoMenu.listaMunicipios);
            }

        }).catch(err => {
            console.log("Error ! ", err)
        })
    }

    /**
     * Llena el menu
     */
    const ChangeDistrito = value => {
        //console.log("escojiste el " + value)
        // verifica que que tipo de Distirto es el que selecciono           

        if (dataResponse.infoMenu.listaDistritosFed != null) {
            //console.log("------",dataResponse.infoMenu.listaDistritosFed[value])
            dataResponse.infoMenu.distritoFedSelec = dataResponse.infoMenu.listaDistritosFed[value];
            setDistrito(dataResponse.infoMenu.listaDistritosFed[value]);
            setPopoverSegundo(entidad.nombreEstado + " - " + dataResponse.infoMenu.listaDistritosFed[value].nombreDistrito)
        }
        if (dataResponse.infoMenu.listaDistritosLoc != null) {
            //console.log("------",dataResponse.infoMenu)
            dataResponse.infoMenu.distritoLocSelec = dataResponse.infoMenu.listaDistritosLoc[value];
            setDistrito(dataResponse.infoMenu.listaDistritosLoc[value])
            setPopoverSegundo(entidad.nombreEstado + " - " + dataResponse.infoMenu.listaDistritosLoc[value].nombreDistrito)

        }
        if (dataResponse.infoMenu.listaMunicipios != null) {
            dataResponse.infoMenu.municipioSelec = dataResponse.infoMenu.listaMunicipios[value];
            setDistrito(dataResponse.infoMenu.listaMunicipios[value])
            setPopoverSegundo(entidad.nombreEstado + " - " + dataResponse.infoMenu.listaMunicipios[value].nombreMunicipio)
        }

        //console.log("Se va enviar ", dataResponse)
        dataResponse.tipoCambioGeografico = 3;
        axios.post(url, dataResponse).then(response => {
            console.log("Exito ! ", response)
            setMenu(response)
            console.log(menu)

        }).catch(err => {
            console.log("error chambiar distro !   ", err)
        });
    }

    /**
     * cambia el valor de los datos 
     */
    const setDatos = () => {
        setEntidad(null)
        setDistrito(null)
        setSelectDistrito(null)
    }

    /**
     * mustra los bototnes 
     */
    const contenidoMenuUsuario = () => {
        return (
            <div>
                <div className="etiqueta">Proceso Electoral</div>
                <Select
                    //defaultValue="Selecciona"
                    style={{ width: 280 }}
                    //onChange={handleChange}
                    value={proceso != null ? proceso.descripcion : "Selecciona"}
                    disabled={dataResponse.infoMenu.listaDetalles.length > 1 ? false : true}
                    onChange={changeProceso}
                >
                    {dataResponse.infoMenu.listaDetalles.map((detalle, id) => (
                        <Option key={id}>{detalle.descripcion}</Option>
                    ))}
                </Select>
                <p> </p>
                {/* 
                    Datos de Entidad 
                    */}
                <div className="etiqueta">Entidad</div>
                <Select
                    //defaultValue="Selecciona"
                    value={entidad != null ? entidad.nombreEstado : "Selecciona"}
                    style={{ width: 280 }}
                    disabled={selectEntidad != null ? false : true}
                    onChange={ChangeEndidad}
                >
                    {selectEntidad != null &&
                        selectEntidad.map((estado, i) => {
                            return (<Option key={i}>{estado.nombreEstado}</Option>)
                        })
                    }
                </Select>
                <p> </p>
                {/* 
                Datos de Distito  
                */}
                <div className="etiqueta">Distrito</div>
                <Select
                    //defaultValue="Selecciona"
                    value={distrito != null ? distrito.nombreDistrito : "Selecciona"}
                    style={{ width: 280 }}
                    disabled={selectDistrito != null && selectDistrito.length > 1 ? false : true}
                    onChange={ChangeDistrito}
                >
                    {selectDistrito != null &&
                        selectDistrito.map((tempDistito, i) => {
                            return (<Option key={i}>{tempDistito.nombreDistrito}</Option>)
                        })
                    }
                </Select>
            </div>
        );
    }

    return (
        <Popover content={contenidoMenuUsuario()} trigger="click">
            <p className="proceso">
                {proceso != null ? proceso.nombreProceso : "Oficinas Centrales"}
                <br />
                {entidad &&
                    <>
                        <Icon type="environment" theme="filled" /> {popoverSegundo}
                    </>
                }
            </p>
        </Popover>
    )
}

