Rama master 

### Installation 

>Requiere [Node.js](https://nodejs.org/) v12.14.1+ para ejecutar. 

>Requiere [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable) v1.22.0+ para ejecutar.

Instalar las dependencias y ejecuatar 

```sh
$cd ine-react 
$yarn install 
$yarn start 
```

